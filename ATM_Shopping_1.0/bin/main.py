# -*-coding: Utf-8 -*-
# @File : main .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
程序的入口
'''

import os
import sys

# 当前文件的绝对路径 ：os.path.dirname(os.path.abspath(__file__))
# E:\Old Boy\day_projects\ATM_Egon\bin
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# 将当前文件的绝对路径加载到全局路径中
sys.path.append(BASE_DIR)

# 从核心代码文件夹(core)引入核心代码文件
# 用户视图层中的代码模块
from core import src

# 定义主程序入口
if __name__ == "__main__":
	# 定义主程序运行方法
	# 先执行用户视图层
	src.main()
