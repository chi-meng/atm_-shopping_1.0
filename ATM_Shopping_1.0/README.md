# 项目的说明书

## 项目名称 : ATM + 购物商城程序

# 项目需求

    1. 额度 15000或自定义  ---> 注册功能（自定义额度）
    2. 实现购物商城，买东西加入 购物车，调用信用卡接口结账  ---> 购物功能 / 支付功能
    3. 可以提现，手续费5%  ---> 提现功能
    4. 支持多账户登录  ---> 登录功能
    5. 支持账户间转账  ---> 转账功能
    6. 记录日常消费流水 ---> 记录流水功能
    7. 提供还款接口  ----> 还款功能
    8. ATM记录操作日志   ---> 记录日志功能
    9. 提供管理接口，包括添加账户、用户额度，冻结账户等。。。  ---> 管理员功能
    10. 用户认证用装饰器 ---> 登录装饰器功能
    11. 每月22号出账单，每月10号为还款日，过期未还，按欠款总额 万分之5 每日计息

## 用户视图层展示给用户选择的功能

    1、注册功能
    2、登录功能
    3、查询余额
    4、提现功能
    5、还款功能
    6、转账功能
    7、查看流水
    8、购物功能
    9、查看购物车
    10、管理员功能

# 一个项目是如何从无到有的

## 一 需求分析

    1.拿到项目，会先到客户那里一起讨论需求，商量项目的功能是否能实现，周期与价格，得到一个需求文档。
    
    2.最后在公司内部需要开一次会议，最终得到一个开发文档
    交给不同岗位的程序员进行开发
        - Python : 后端 / 爬虫
        - 不同的岗位 : 
            - UI 界面设计 : 
            
            - 前端 :
                - 拿到UI交给他的图片,搭建网页
                - 设计一些页面中,哪些位置需要接收数据,需要进行数据交互
    
            - 后端 : 
                - 直接核心的业务逻辑，调整数据库进行数据库的增删改查
    
            - 测试 : 
                - 会给代码进行全面测试
                    - 比如压力测试、界面测试(CF卡箱子)
    
            - 运维 : 
                - 搭建项目架构
                - 部署项目

## 二 程序的架构设计

### 1、程序设计的好处

    1） 思路清晰
    2） 不会出现写一般代码时推翻重写
    3） 方便自己或同时进行代码维护

### 2、架构图设计

![ATM架构图](imgs/README.assets/ATM架构图.png)

### 3、三层架构的好处

```
1） 把每个功能都分层三部分，逻辑清晰
2） 如果用户更换不用的用户界面或不同的数据储存机制都不会影响接口层的核心逻辑代码，扩展性强
3） 可以在接口层，准确的记录日志和流水
```

#### 一 用户视图层

```
用于与用户交互，可以接收用户的输入，打印接口返回的数据。
```

#### 二 逻辑接口层

```
接受用户层传过来的参数，根据逻辑判断调用数据层加以处理
并返回一个结果给用户视图层
```

#### 三 数据处理层

```
接收接口层传递过来的参数，做数据处理
    - 保存数据 save()
    - 查看数据 select()
    - 更新数据
    - 删除数据
```

## 三 分任务开发

## 四 测试代码

## 五 上线运营

# 统计代码插件

    file ---> settings ---> Plugins ---> Statistics

# 【一】小结一

```
	一 一个项目是如何从无到有的
		-1、需求分析（提取功能）
		-2、程序的架构设计（核心）
		-3、分任务开发
		-4、项目测试
		-5、项目上线
    二 程序的架构设计
    	- 三层架构
    		- 用户视图层
    			- 用于与用户进行交互
    			- 打印输出内容给用户
    		- 逻辑接口层
    			- 核心业务逻辑代码，相当于用户视图与数据处理层的桥梁
    			- 返回结果给视图层
    		- 数据处理层
    			- 做数据处理	
    				- 增
    				- 删
    				- 改
    				- 查
    三 分层设计的优点
    	- 思路清晰
    	- 不会推翻重写
    	- 扩展性强
    	- 便于维护
    
    四 搭建项目
    	- ATM 项目根目录
    		- README.md 项目说明书
    		
    		- start.py 项目启动文件的创建方式二，直接暴露在最外层
    		
    		- conf 配置文件
    			- settings.py
    		
    		- lib 公共方法文件
    			- common.py
    			
    		- core（用户视图层） 存放用户视图层代码文件
    			- src.py
    			
    		- interface（逻辑接口层） 存放核心业务逻辑代码
    			- user_interface.py 用户相关接口
    			- bank_interface.py 银行相关接口
    			- shop_interface.py 购物相关接口
    			
    		- db（数据处理层） 存放数据与数据处理层代码
    			- db_hander.py 数据处理层代码
    			- user_data 用户数据
    			
    		- log 存放日志文件
    		
    		- bin（启动文件目录） 存放启动文件的方式一，单独创建目录
            	- main.py
    
    
    		
    	
```

