# -*-coding: Utf-8 -*-
# @File : admin .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/6

from core import src
from interface import admin_interface, shop_interface
from db import db_hander


# 添加新用户
def add_user():
	# 调用用户视图层的注册函数
	src.register()


# 修改用户额度
def change_balance():
	'''
	# （1） 管理员输入修改额度金额
	# （2） 输入修改的目标用户
	# （3） 调用银行接口修改额度
	:return:
	'''
	while True:
		# （1） 管理员输入修改的目标用户
		change_to_user = input('请输入需要修改额度的用户:>>>>').strip()

		# （2） 输入修改的目标用户的额度
		money = input('请输入要修改的额度:>>>>').strip()
		if not money.isdigit():
			continue

		# （3） 调用银行接口修改额度
		# (True, ->用户[{change_to_user}]--->额度修改成功!--->当前额度为[{money}$]<-)
		flag, msg = admin_interface.change_balance_interface(change_to_user, money)
		if flag:
			print(msg)
			break
		else:
			print(msg)


# 冻结账户
def locked_user():
	'''
	# （1）输入被锁定的目标用户
	:return:
	'''
	while True:
		# （1） 管理员输入冻结的目标用户
		lock_user = input('请输入需要冻结的用户:>>>>').strip()
		flag, msg = admin_interface.lock_user_interface(lock_user)
		if flag:
			print(msg)
			break
		else:
			print(msg)


# 初始化购物车列表
def init_shopping_list():
	good_dict_first = {
		0: ['梦梦的星空水晶球', 99],
		1: ['简约梦梦二次元纯棉T恤', 159],
		2: ['梦梦二次元萌物计算器', 29],
		3: ['梦梦小恶魔双面钥匙扣', 39],
		4: ['梦梦趣味充电宝', 79],
		5: ['小梦梦系列口红', 69],
		6: ['梦梦的星星拖鞋', 49],
		7: ['梦梦的星空耳机', 79],
		8: ['二次元梦梦睡衣套装', 129],
		9: ['梦梦的星光手环', 49]
	}

	db_hander.save(good_dict_first, roles='admin')


# 添加商品数据
def add_goods():
	while True:
		'''
			（1）拿到管理员传入的商品编号 ， 商品名字 ， 商品价格
			（2）对商品编号进行校对是否存在该商品编号
			（3）不存在商品编号则进行数据的录入
			:param good_number: 商品编号
			:param good_name: 商品名字
			:param good_price: 商品价格
			:return:
			'''
		# 调用数据处理层拿到商品数据
		good_dict = db_hander.select(username=None, roles='admin')
		good_number = input('请输入要添加的商品编号:>>>').strip()
		if int(good_number) in good_dict.keys():
			print('编号已存在,请重新输入编号!')
			continue
		else:
			good_name = input('请输入要添加的商品名字:>>>').strip()
			good_price = input('请输入要添加的商品价格:>>>').strip()
			good_dict[int(good_number)] = [good_name, good_price]
			flag, msg = shop_interface.add_goods_to_data(good_number, good_dict)
			if True:
				print(msg)
				break


# 删除商品数据


def del_goods():
	while True:
		# 调用数据处理层拿到商品数据
		good_dict = db_hander.select(username=None, roles='admin')
		good_number = input('请输入要删除的商品编号:>>>').strip()
		flag, msg = shop_interface.del_goods_interface(good_number, good_dict)
		if flag:
			print(msg)

			break
		else:
			print('删除失败')


admin_menus = '''
    ======= 管理员功能菜单 =======
            1: 添加用户
            2: 调整余额
            3: 冻结账户
            4: 初始化商品数据
            5: 添加商品未完善
            6: 删除商品未完善
    ==========  RND ==========
'''

# 创建管理员功能入口字典
admin_dict = {
	'1': add_user,
	'2': change_balance,
	'3': locked_user,
	'4': init_shopping_list,
}


# 定义管理员程序入口
def admin_main():
	while True:
		print(admin_menus)
		choice = input('请输入管理员功能ID!(q退出)').strip()
		if choice == 'q':
			break
		elif choice not in admin_dict:
			print('请输入正确的功能代码:>>>>')
			continue

		# func_dict.get('1')() ----> add_user()
		admin_dict.get(choice)()
