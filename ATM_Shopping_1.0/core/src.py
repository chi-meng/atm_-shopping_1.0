# -*-coding: Utf-8 -*-
# @File : src .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
存放用户视图层
'''
# 导入用户接口层
from interface import user_interface, bank_interface, shop_interface
# 从公用方法中导入认证装饰器
from lib import common
# 导入管理员接口层
from core import admin as admin_src
# 导入购物车接口
from core import shop

func_menus = '''
    ======= ATM + 购物车 =======
            1: 注册功能
            2: 登录功能
            3: 查询余额
            4: 提现功能
            5: 还款功能
            6: 转账功能
            7: 查看流水
            8: 购物功能
            9: 查看购物车
            10: 管理员功能
    ==========  RND ==========
'''

# 全局变量，存储当前登录的全局用户信息

login_user = None


# 1: 注册功能
# 分层版本
def register():
	while True:
		# 第一层面板
		# （1）让用户输入用户名和密码进行校验
		username = input('请输入用户名:>>>>').strip()
		password = input('请输入密码:>>>>').strip()
		re_password = input('请再次确认密码:>>>>').strip()
		# 可以输入自定义的金额

		# 逻辑处理 ： 校验两次密码是否一致
		if password == re_password:
			# （2）调用接口层的注册接口，将用户名与密码交给接口层进行处理

			# 拿到的元祖 ---> （False,'用户已存在!'）
			# 解压赋值 flag,msg ---> （flag ---> False, msg ---> '用户已存在!'）

			# (True,'用户注册成功') (False,'用户已存在')
			flag, msg = user_interface.register_interface(username, password)

			# 根据flag判断用户注册是否成功 ---> 控制循环是否退出
			# 注册成功则打印注册成功信息并终止循环退出
			if flag:
				print(msg)
				break
			# 注册不成功则二次注册
			else:
				print(msg)
		else:
			print('密码不一致，请重新输入!')
			continue


# 2: 登录功能
def login():
	while True:
		# （1）让用户输入用户名和密码
		username = input('请输入用户名:>>>>').strip()
		password = input('请输入密码:>>>>').strip()

		# （2）调用接口层，将数据传给登录接口
		#  (True, f'用户:{username}>>>>登陆成功!'),(False, '密码错误'),(False, '用户不存在,请重新输入!!')
		flag, msg = user_interface.login_interface(username, password)

		if flag:
			print(msg)
			global login_user
			login_user = username
			break
		else:
			print(msg)


# 3: 查询余额
# 语法糖：认证装饰器
@common.login_auth
def check_Balance():
	# （1）从接口层直接调用查看余额接口
	balance = user_interface.check_balance_interface(login_user)

	print(f'用户:>>{login_user} 账户余额为:>> {balance}￥')


# 4: 提现功能
# 语法糖：认证装饰器
@common.login_auth
def withdraw():
	while True:
		# （1）让用户输入提现金额
		input_money = input('请输入提现金额::>>>>').strip()

		# （2）判断用户金额是否为数字
		if not input_money.isdigit():
			print('请确认你输入的金额::>>>>')
			continue

		# （3）用户提现金额,交给接口层处理
		flag, msg = bank_interface.withdraw_interface(login_user, input_money)

		# (4) 根据返回的结果打印相应的提示语句
		if flag:
			print(msg)
			break
		else:
			print(msg)


# 5: 还款功能
# 语法糖：认证装饰器
@common.login_auth
def repay():
	'''
	银行卡还款，无论是信用卡还是储蓄卡，都是能充值任意大小的金额
	:return:
	'''
	while True:
		# （1）让用户输入还款金额
		input_money = input('请输入还款金额:>>>>').strip()

		# （2）判断用户输入的是否为数字
		if not input_money.isdigit():
			print('请输入正确的金额!!')
			continue

		# （2.1） 纠正用户输入的金额大于 0
		input_money = int(input_money)
		if input_money > 0:
			# （3） 调用银行还款接口进行还款
			flag, msg = bank_interface.repay_interface(login_user, input_money)
			if flag:
				print(msg)
				break

		else:
			print('输入的金额不能小于 0 ')


# 6: 转账功能
# 语法糖：认证装饰器
@common.login_auth
def transfer():
	'''
	1、接受用户输入的转账金额
	2、接受用户输入的转账目标用户
	:return:
	'''
	while True:
		# (1) 让用输入转账对方账户与转账金额
		to_user = input('请输入转账目标账户:>>>>').strip()
		money = input('请输入转账金额:>>>>').strip()

		# （2）判断用户输入的金额是否为数字且 > 0
		if not money.isdigit():
			print('请输入正确的金额!!')
			continue

		# （2.1）判断输入的金额大于0
		money = int(money)
		if money > 0:
			# （3）调用转账接口 ---> 当前用户的信息,目标用户信息,转账金额
			flag, msg = bank_interface.transfer_interface(login_user, to_user, money)
			if flag:
				print(msg)
				break
			else:
				print(msg)


		else:
			print('金额必须大于0!!')


# 7: 查看流水
# 语法糖：认证装饰器
@common.login_auth
def check_flow():
	# 查看流水接口
	flow_list = bank_interface.check_flow_interface(login_user)

	# 判断返回的流水列表是否存在流水
	# 如果存在流水记录则打印每一条流水记录
	if flow_list:
		for flow in flow_list:
			print(flow)
	else:
		print('当前用户无流水记录!!')


# 8: 购物功能
# 语法糖：认证装饰器
@common.login_auth
def shopping():
	'''
	文件作为数据库
	:return:
	'''
	shop.shop_main(login_user)


# 9: 查看购物车
# 语法糖：认证装饰器
@common.login_auth
def check_shop_car():
	# 直接调用查看购物车接口
	shop_car = shop_interface.check_shop_car_interface(login_user)
	# {'梦梦的星光手环': [49, 8], '二次元梦梦睡衣套装': [129, 5], '梦梦的星星拖鞋': [49, 1], '小梦梦系列口红': [69, 1]}
	if shop_car:
		for good_name, good_price_number in shop_car.items():
			good_num = good_price_number[0]
			good_price = good_price_number[1]
			print(
				f' **** 当前用户购买的物品:>>>{good_name} ---- 数量:>>>[{good_num}个] ---- 单价:>>>[{good_price}$] ---- 总价:>>>[{good_price * good_num}$] ****')
	else:
		print('当前购物车无商品存在!')

# 10: 管理员功能
# 语法糖：认证装饰器
@common.login_auth
def admin():
	# 调用接口 管理员功能入口
	admin_src.admin_main()


# 创建函数功能字典
func_dict = {
	'1': register,
	'2': login,
	'3': check_Balance,
	'4': withdraw,
	'5': repay,
	'6': transfer,
	'7': check_flow,
	'8': shopping,
	'9': check_shop_car,
	'10': admin,

}


# 视图层主程序
def main():
	while True:
		print(func_menus)
		choice = input('请选择功能编号:>>>>').strip()
		if choice not in func_dict:
			print('请输入正确的功能代码:>>>>')
			continue

		# func_dict.get('1')() ----> register()
		func_dict.get(choice)()


if __name__ == '__main__':
	main()
