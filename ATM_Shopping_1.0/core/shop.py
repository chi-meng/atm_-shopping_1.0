# -*-coding: Utf-8 -*-
# @File : shop .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/7

from interface import shop_interface
from db import db_hander


# 初始化商品数据详情
def check_goods_list():
	# （1）初始化商品列表
	goods_list = []

	# （2）代用数据处理层拿到商品数据字典
	good_dict = db_hander.select(username=None, roles='admin')

	# （3）将数据字典中的数据转换成列表的形式，便于下边使用
	# （3.1）如果字典数据存在，返回物品列表
	if good_dict:
		for good in good_dict.values():
			goods_list.append(good)

		return goods_list


# （3.2）商品数据字典不存在则提醒管理员添加数据


goods_list = check_goods_list()


def show_goods(goods_list):
	# 枚举 enumerate(可迭代对象) 方法 ----> （可迭代对象的索引，索引对应的值）
	# 枚举 enumerate(可迭代对象) 方法 ----> （0，['梦梦的星空水晶球', 99]）
	print("========================== 欢迎来到梦梦趣味商城 ============================")
	for index, goods in enumerate(goods_list):
		# 解压赋值,将商品信息全都解放出来
		shop_name, shop_price = goods
		# 打印商品列表
		print(f'-> --- 商品编号:[{index}] --- 商品名称:[{shop_name}] --- 商品单价:[{shop_price}] --- <-')
	print("=========================== 梦梦趣味商城欢迎你 ============================")


# 添加购物车功能
def add_shopping_car(login_user, shopping_car):
	# 判断当前用户是否添加过购物车
	if not shopping_car:
		# continue
		return False, '-> 购物车不能为空! <-'
	# (7) 调用添加购物车接口
	flag, msg = shop_interface.add_good_car_interface(login_user, shopping_car)
	return flag, msg


# 支付结算功能
def settlement_shopping_cat(login_user, shopping_car):
	# 判断当前用户是否添加过购物车
	if not shopping_car:
		# 调用添加购物车接口
		# continue
		return False, '-> 购物车不能为空!不能支付!请重新输入! <-'
	# (6) 调用支付接口进行支付
	flag, msg = shop_interface.shopping_interface(login_user, shopping_car)
	return flag, msg


def add_goods_to_shopping_car(choice, shopping_car):
	# （4） 获取商品名称与单价
	good_name, good_price = goods_list[choice]

	# （5） 加入购物车  ---->  获取购物车
	# （5.1） 判断用户选择的商品是否重复，重复则数量 +1
	if good_name in shopping_car:
		# 已经存在的商品名称，取他的数量 +1
		# shopping_car['good_name'][1] ---->  '数量'
		shopping_car[good_name][1] += 1

	else:
		# 否则 默认 数量 为 1
		# {'商品名称':['单价','数量']}
		shopping_car[good_name] = [good_price, 1]

	print(f'-\* 当前购物车:>>>{shopping_car} */-')


def shop_main(login_user):
	# 初始化当前购物车
	# {'商品名称':['单价','数量']}
	shopping_car = {}
	while True:
		if goods_list:
			# （1）先打印商品信息，让用户选择
			show_goods(goods_list)

			# （2） 用户根据商品编号进行商品的选择
			choice = input('-> 请输入商品对应的编号(是否结算请输入y/n):>>>> <-').strip()

			# （2.1）输入的是y进入支付结算功能
			if choice == 'y':
				# 对自己的购物车进行结算功能
				flag, msg = settlement_shopping_cat(login_user, shopping_car)
				if flag:
					print(msg)
					break
				else:
					print(msg)

			# （2.2）输入的是n添加购物车
			elif choice == 'n':
				# 将购物车写入到自己的数据中
				flag, msg = add_shopping_car(login_user, shopping_car)
				if flag:
					print(msg)
					break
				else:
					print(msg)

			if not choice.isdigit():
				print('请输入正确的编号!')
				continue

			choice = int(choice)
			# （3） 判断choice是否存在
			# 将选项转为整型，进行商品选购
			# 求当前列表的最大长度，在最大长度内的就可以使用
			if choice not in range(len(goods_list)):
				print('请输入正确的编号!')
				continue

			# 将商品添加到购物车功能
			add_goods_to_shopping_car(choice, shopping_car)
		else:
			print('请联系管理员添加商品')
			break
