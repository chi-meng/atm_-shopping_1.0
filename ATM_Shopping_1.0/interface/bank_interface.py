# -*-coding: Utf-8 -*-
# @File : bank_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
银行相关业务接口
'''
from db import db_hander
from lib import common

user_logger = common.get_logger('bank')


# 提现接口(提现费5%)
def withdraw_interface(username, input_money):
	# （1）先获取用户字典
	user_dict = db_hander.select(username)
	# 得到的用户金额为账户中的余额
	balance = int(user_dict.get('balance'))

	# 本金 + 手续费 ---> float 类型
	money = int(input_money) * 1.05

	# 判断用户金额是否足够
	if balance >= money:
		# （2）修改用户字典中的金额
		# （2.1）修改后额金额
		balance -= money

		# （2.2）更新用户字典中的余额
		user_dict['balance'] = balance

		# （3）记录流水
		flow = f'-> 用户[{username}]:--->提现金额为[{input_money}$]:--->手续费为[{money - float(input_money)}$] <-'

		# （）记录日志
		user_logger.info(flow)

		user_dict['flows'].append(flow)

		# （4）再保存数据，或更新数据
		db_hander.save(user_dict)
		return True, flow

	return False, f'余额不足,请确认金额!'


# 用户还款接口
def repay_interface(username, money):
	'''
	（1）获取到用户的金额
	（2）给用户的金额增加金额
	:return:
	'''
	# （1）获取用户字典
	user_dict = db_hander.select(username)

	# （2）不需要处理用户剩余余额，只需要进行增加金额
	# user_dict['balance'] ----> int类型
	user_dict['balance'] += money

	# (3) 记录流水
	flow = f'-> 用户[{username}]:--->还款金额为[{money}$]:--->剩余金额为[{user_dict["balance"]}$] <-'

	# （）记录日志
	user_logger.info(flow)

	user_dict['flows'].append(flow)

	# （4）调用数据处理层，进行用户数据的数据更新
	db_hander.save(user_dict)

	# 还款成功返回信息给用户视图层
	return True, flow


# 转账接口
def transfer_interface(my_user, to_user, money):
	'''
	1、先获取当前用户数据
	2、获取目标用户数据
	3、转账金额
	:return:
	'''
	# （1）获取当前用户的字典
	my_user_dict = db_hander.select(my_user)

	# （2）获取目标用户的字典
	to_user_dict = db_hander.select(to_user)

	# （3）判断目标用户是否存在
	if not to_user_dict:
		return False, '目标用户不存在!'

	# （4）用户存在，判断当前用户的金额是否足够
	if my_user_dict['balance'] >= money:
		# （5）若余额足够，则开始给目标用户转账
		# （5.1）给当前用户的余额减去金额
		my_user_dict['balance'] -= money
		# （5.2）给目标用户的数据增加金额
		to_user_dict['balance'] += money

		# (5.3) 记录流水
		# （5.3.1）记录当前用户的流水
		my_flow = f'-> 用户[{my_user}]:--->向目标用户[{to_user}]:--->转账[{money}$]:--->成功!! <-'

		# （）记录日志
		user_logger.info(my_flow)

		my_user_dict['flows'].append(my_flow)
		# （5.3.2）记录对方用户的流水
		to_flow = f'-> 用户[{to_user}]:--->接收目标用户[{my_user}]:--->收款[{money}$]:--->成功!! <-'

		# （）记录日志
		user_logger.info(to_flow)

		to_user_dict['flows'].append(to_flow)

		# （6）保存用户数据
		# （6.1）调用数据处理层的save接口，保存当前用户数据
		db_hander.save(my_user_dict)
		# （6.2）调用数据处理层的save接口，保存目标用户数据
		db_hander.save(to_user_dict)

		return True, my_flow

	return False, '当前用户余额不足,无法进行转账!!'


# 查看流水接口
def check_flow_interface(login_user):
	# 根据用户名调用数据处理接口，拿到用户字典
	user_dict = db_hander.select(login_user)

	# 返回用户字典中的流水列表
	return user_dict.get('flows')


# 购物商城支付接口
def pay_interface(login_user, cost):
	user_dict = db_hander.select(login_user)

	# （1）判断用户金额是否足够
	if user_dict['balance'] >= cost:
		# （2）将用户中的余额进行减钱操作
		user_dict['balance'] -= cost
		flow = f'-> 当前用户:>{login_user} ----> 消费金额为:>{cost}$ <-'

		# （）记录日志
		user_logger.info(flow)

		# （3）添加流水记录
		user_dict['flows'].append(flow)

		# 支付成功的同时将数据中的购物车清空，否则二次登录会显示购物车数据
		user_dict['shop_car'] = {}

		# （4） 进行数据的保存
		db_hander.save(user_dict)

		# 支付成功返回结果
		return True
	# 支付失败返回结果
	return False
