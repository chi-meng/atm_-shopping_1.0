# -*-coding: Utf-8 -*-
# @File : shop_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
购物商城接口
'''

# 导入银行接口
from interface import bank_interface
from db import db_hander
from lib import common

# 根据接口类型不同传进去不同的日志对象
user_logger = common.get_logger('shop')


# 商城准备结算接口
def shopping_interface(login_user, shopping_car):
	'''

	:param login_user:
	:param shopping_car:
	:return:
	'''
	# （1）计算消费总额
	# shopping_car ： {'商品名称':['单价','数量']}
	cost = 0
	# 取字典对应的value值
	for price_number in shopping_car.values():
		# 解压赋值 ： ['单价','数量']
		price, number = price_number

		# 总价 = 单价 * 数量
		cost += price * number

	# (2)逻辑校验成功后再去进行银行接口进行结算 --- 用户名,消费总额
	flag = bank_interface.pay_interface(login_user, cost)
	if flag:
		flow = f'-> [{login_user}]:>>支付[{cost}$]成功!!---->准备发货! <-'
		# （）记录日志
		user_logger.info(flow)

		return True, flow

	return False, '-> 支付失败---->请重新确认 <-'


# 购物车添加接口
def add_good_car_interface(login_user, shopping_car):
	# （1） 先获取当前用户的购物车
	user_dict = db_hander.select(login_user)

	# 获取用户字典中的 shop_car
	shop_car = user_dict.get('shop_car')

	# (2) 添加购物车
	# （2.1） 判断当前用户选择的商品是否已经存在
	# shopping_car --- > {'商品名称':['单价','数量']}
	for good_name, good_price_number in shopping_car.items():
		# 商品的数量
		number = good_price_number[1]

		# (2.2) 若商品重复，则累加商品数量
		if good_name in shop_car:
			# 累加商品数量
			user_dict['shop_car'][good_name][1] += number
		else:
			# （2.3） 若不是重复的商品,自动更新到字典中
			user_dict['shop_car'].update(
				{good_name: good_price_number}
			)

		# 保存用户数据
		db_hander.save(user_dict)

	return True, '-> 添加购物车成功! <-'


# 查看购物车接口
def check_shop_car_interface(login_user):
	user_dict = db_hander.select(login_user)

	return user_dict['shop_car']


# 添加商品数据接口
def add_goods_to_data(good_number, good_dict):
	db_hander.save(data=good_dict, roles='admin')
	flow = f'当前商品{good_number}添加成功!'

	return True, flow


# 删除商品数据接口
def del_goods_interface(good_number, good_dict):
	# {"0": ["梦梦的星空水晶球", 99], "1": ["简约梦梦二次元纯棉T恤", 159], "2": ["梦梦二次元萌物计算器", 29], "3": ["梦梦小恶魔双面钥匙扣", 39], "4": ["梦梦趣味充电宝", 79], "5": ["小梦梦系列口红", 69], "6": ["梦梦的星星拖鞋", 49], "7": ["梦梦的星空耳机", 79], "8": ["二次元梦梦睡衣套装", 129], "9": ["梦梦的星光手环", 49]}
	good_dict.pop(good_number)
	db_hander.save(data=good_dict, roles='admin')
	flow = f'删除当前商品{good_dict[good_number]}成功!'

	return True, flow
