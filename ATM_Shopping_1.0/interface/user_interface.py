# -*-coding: Utf-8 -*-
# @File : user_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
逻辑
用户接口层
'''
from db import db_hander
from lib import common

user_logger = common.get_logger('user')


# 注册接口
def register_interface(username, password, balance=15000):
	# （2）校验用户是否存在
	# （2.1）调用数据处理层中的 select 函数,会返回专用户字典 或 None
	user_dict = db_hander.select(username)

	# {user:user,pwd:pwd....} or None
	# 若用户存在则return，告诉用户重新输入
	if user_dict:
		return False, '用户已存在!'  # ---> （False,'用户已存在!'）

	# （3）若用户不存在，则保存用户数据
	# 做密码加密
	# (encrypt_result, salt)
	# 加把盐随机六位大小写验证码
	salt = common.get_verify_code(6)
	password = common.encrypt_decrypt(password, salt)
	# （3.1）组织用户的数据的字典信息
	user_dict = {
		'username': username,
		'password': password,
		'salt': salt,
		'balance': balance,
		# 用于记录用户流水的列表
		'flows': [],
		# 用于记录用户购物车
		'shop_car': {},
		# 用户是否被冻结，默认为False
		# False : 未被冻结
		# True : 被冻结
		'locked': False,
	}

	# （3.2）保存数据
	db_hander.save(user_dict)

	# （3.3）记录日志
	msg = f'{username}:>>>>注册成功'
	user_logger.info(msg)
	return True, msg


# 登录接口
def login_interface(username, password):
	# （1）先查看当前数据是否存在
	# {用户数据字典} or None
	user_dict = db_hander.select(username)
	# 用于判断用户是否存在

	# （2）判断用户是否存在
	if user_dict:
		# 判断用户状态是否处于冻结状态
		if user_dict.get('locked'):
			return False, f'-> 当前账户{username}--->已被锁定---> 请联系管理员解除 <-'
		# 登录时给用户输入的密码进行一次加密
		# 从用户字典里面拿出盐
		salt = user_dict.get('salt')
		# 将输入的密码（定值）和盐（定值）发到加密方法里加密密码
		password = common.encrypt_decrypt(password, salt)
		# （3） 校验密码是否一致
		if password == user_dict.get('password'):

			# （3.1）记录日志
			msg = f'用户:{username}>>>>登陆成功!'
			user_logger.info(msg)
			return True, msg
		else:

			# （3.2）记录日志
			msg = f'-> 用户:[{username}]>>>>密码错误! <-'
			info_msg = f'-> 用户:[{username}]>>>>密码>>>>[{password}]错误!>>>>当前盐[{salt}] <-'
			user_logger.warning(info_msg)
			return False, msg

	# 用户不存在则返回 False
	# （）记录日志
	msg = f'-> {username}用户不存在 ---> 请重新输入!! <-'
	return False, msg
#  (True, f'用户:{username}>>>>登陆成功!'),(False, '密码错误'),(False, '用户不存在,请重新输入!!')

# 查看余额接口
def check_balance_interface(username):
	# 调用数据处理层 根据用户名返回用户信息字典
	user_dict = db_hander.select(username)

	# 返回用户信息字典中的余额信息，而不是全部信息
	return user_dict['balance']
