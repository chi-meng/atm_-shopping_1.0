# -*-coding: Utf-8 -*-
# @File : admin_interface.py .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/6

# 导入数据处理层的数据处理接口
from db import db_hander
from lib import common

user_logger = common.get_logger('admin')

# 修改目标用户额度接口
def change_balance_interface(change_to_user, money):
	user_dict = db_hander.select(change_to_user)

	# 判断用户字典是否为空
	if user_dict:
		# 如果不为空，则进行额度替换
		user_dict['balance'] = int(money)

		# 调用数据处理层保存数据
		db_hander.save(user_dict)

		msg = f'-> 用户[{change_to_user}]--->额度修改成功!--->当前额度为[{money}$] <-'
		# （）记录日志
		user_logger.info(msg)

		return True, msg

	return False, '-> 当前用户信息不存在,请确认信息!! <-'


# 冻结目标用户接口
def lock_user_interface(username):
	# 调用数据处理层拿到用户字典
	user_dict = db_hander.select(username)

	# 判断用户是否存在
	if user_dict:
		# 用户存在
		# 读取用户字典中的用户锁定状态进行更改,将默认False修改为True
		user_dict['locked'] = True

		# 调用数据处理层进行数据的覆写
		db_hander.save(user_dict)

		msg = f'-> 当前用户{username}--->冻结成功!!!  <-'
		# （）记录日志
		user_logger.info(msg)

		return True, msg

	return False, f'-> 当前用户{username}--->不存在!!! <-'
