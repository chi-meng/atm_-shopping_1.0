# -*-coding: Utf-8 -*-
# @File : common .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
存放公共方法
'''
import random
import hashlib
from core import src
import logging
import logging.config
from conf import settings


# 随机加盐
def get_verify_code(n):
	'''加把盐 生成六位随机 （数字 + 大小写） 验证码'''
	salt = ''
	for i in range(n):
		random_int = str(random.randint(0, 9))  # 0-9之间的整数
		random_upper = chr(random.randint(65, 90))  # A-Z之间的字母
		random_lower = chr(random.randint(97, 122))  # a-z之间的字母
		temp = random.choice([random_int, random_upper, random_lower])
		salt += temp
	return salt


# 密码MD5加密
def encrypt_decrypt(password, salt):
	# 转为二进制数据
	# 拼接数据 盐 + 密码
	data = salt + password
	# 将加盐后的数据转码
	data = data.encode('utf-8')
	# 创建md5对象
	md5 = hashlib.md5()
	# md5进行加密
	md5.update(data)
	# 取出md5加密后的哈希值
	encrypt_result = md5.hexdigest()
	# 将盐和加密后的密码返回
	return encrypt_result


# 登录认证装饰器
def login_auth(func_name):
	def inner(*args, **kwargs):
		# 如果登录函数的全局变量已经有值了则表示已经登陆成功
		if src.login_user:
			res = func_name(*args, **kwargs)
			return res
		else:
			print('用户未登录,请登录后享受幸福人生!!')
			src.login()

	return inner


# 添加日志功能:日志功能在接口层使用
def get_logger(log_type):
	'''
	logger_type : 日志类型（银行日志/user日志/bank日志/购物商城日志）
	:return:
	'''
	# （1） 加载日志配置信息
	logging.config.dictConfig(settings.LOGGING_DIC)

	# （2）获取日志对象
	logger = logging.getLogger(log_type)

	return logger