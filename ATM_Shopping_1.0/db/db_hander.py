# -*-coding: Utf-8 -*-
# @File : db_hander .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/5

'''
数据处理层
    - 专门用于处理数据
'''

# 导入模块
import json
# 导入配置文件
from conf import settings
import os


# 查看函数
def select(username=None, roles=None):
	path = None
	if username != None and roles == None:
		# （1）接收接口层传过来的 username 用户名
		# 拼接用户文件数据文件路径
		path = os.path.join(settings.USER_DATA_PATH, f'{username}.json')
	elif roles == 'admin':
		path = os.path.join(settings.GOOD_DATA_PATH, 'goos.json')

	# （2）校验用户json文件是否存在
	if os.path.exists(path):
		# （3） 打开数据,并返回给接口层
		with open(path, 'r', encoding='utf8') as f:
			data = json.load(f)
			return data


# （4）如果用户不存在则默认返回一个None
# return None

# 保存数据（添加新数据或者更新数据）
def save(data=None, roles=None):
	path = None
	if roles == None:
		# （1）构建用户数据
		# 用户数据 , 以用户名作为每个用户数据的存储数据文件
		# 从用户数据字典中拿到用户名
		username = data.get('username')
		# username.json
		# 拼接用户文件数据文件路径
		path = os.path.join(settings.USER_DATA_PATH, f'{username}.json')
	elif roles == 'admin':
		path = os.path.join(settings.GOOD_DATA_PATH, 'goos.json')

	# （2）保存用户数据
	with open(path, 'w', encoding='utf-8') as f:
		# 存储用户数据
		# ensure_ascii=False : 让文件中的中文显示更美观，否则为二进制数据
		json.dump(data, f, ensure_ascii=False)
