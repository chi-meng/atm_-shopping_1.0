# ATM_Shopping_1.0

#### 介绍
上传我写的一个ATM + 购物车项目的初代版本，任然存在很多课调教的地方

#### 软件架构
软件架构说明

- ATM 项目根目录
    - README.md 项目说明书
    
    - imgs README.md的图片文件夹
		
    - conf 配置文件
        - settings.py
		
    - lib 公共方法文件
        - common.py

			
    - core（用户视图层） 存放用户视图层代码文件
        - admin.py 管理员入口
        - src.py  核心代码逻辑
        - shop.py 商城入口
			
    - interface（逻辑接口层） 存放核心业务逻辑代码
        - user_interface.py 用户相关接口
        - bank_interface.py 银行相关接口
        - shop_interface.py 购物相关接口

    - db（数据处理层） 存放数据与数据处理层代码
        - db_hander.py 数据处理层代码
        - user_data 用户数据
            - username.json 用户详细数据
        - goods_data 商品详情数据
            - goods.json 商品详细数据
			
    - log 存放日志文件
		
    - bin（启动文件目录） 存放启动文件
        - main.py


#### 安装教程

1.  下载源码压缩包
2.  解压压缩包
3.  自带虚拟环境

#### 使用说明

1.  基于py3.6/3.8运行
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
