# ATM_Shopping_2.0 演示文档

## 【前言】

> 本文档只演示 2.0 版本的各种功能展示
>
> 详细的思路请看 版本 1.0

> ATM_Shopping_1.0：https://gitee.com/chi-meng/atm_-shopping_1.0/tree/master/ATM_Shopping_1.0

> ATM_Shopping_2.0：



## 【一】文档目录展示

![1](./imgs/ATM_Shopping_2.0演示文档.assets/1.png)

## 【二】用户功能展示

```
===================用户功能菜单=====================
                  1.注册
                  2.登陆
                  3.取款
                  4.转账
                  5.充值余额
                  6.查看流水
                  7.查看银行信息
======================欢迎使用=======================
```

![2](./imgs/ATM_Shopping_2.0演示文档.assets/2.png)

### 【1】注册

![3](./imgs/ATM_Shopping_2.0演示文档.assets/3.png)

- 用户信息展示

![4](./imgs/ATM_Shopping_2.0演示文档.assets/4.png)

### 【2】登陆

![5](./imgs/ATM_Shopping_2.0演示文档.assets/5.png)

### 【3】取款

- 默认余额10000

![6](./imgs/ATM_Shopping_2.0演示文档.assets/6.png)

- 提现后余额

![7](./imgs/ATM_Shopping_2.0演示文档.assets/7.png)

### 【4】转账

- 注册第二用户
  - 记录银行卡号为`*131452153*`

![8](./imgs/ATM_Shopping_2.0演示文档.assets/8.png)

![9](./imgs/ATM_Shopping_2.0演示文档.assets/9.png)

- 当余额 + 手续费不能满足转账时
  - 不允许转账 

![10](./imgs/ATM_Shopping_2.0演示文档.assets/10.png)

- 满足条件时
  - 允许转账

![11](./imgs/ATM_Shopping_2.0演示文档.assets/11.png)

### 【5】充值余额

![12](./imgs/ATM_Shopping_2.0演示文档.assets/12.png)

### 【6】查看流水

![13](./imgs/ATM_Shopping_2.0演示文档.assets/13.png)

### 【7】查看银行信息

![14](./imgs/ATM_Shopping_2.0演示文档.assets/14.png)

## 【二】管理员功能展示

```
===================管理员功能菜单=====================
                  1.注册
                  2.登陆
                  3.冻结用户
                  4.修改用户余额
                  5.添加商品
                  6.删除商品
                  7.修改商品
                  8.初始化商品列表
                  9.查看全部商品列表
======================欢迎使用=======================
```

### 【1】注册

![15](./imgs/ATM_Shopping_2.0演示文档.assets/15.png)

### 【2】登陆

![16](./imgs/ATM_Shopping_2.0演示文档.assets/16.png)

### 【3】冻结用户

![17](./imgs/ATM_Shopping_2.0演示文档.assets/17.png)

![18](./imgs/ATM_Shopping_2.0演示文档.assets/18.png)

### 【4】修改用户余额

![19](./imgs/ATM_Shopping_2.0演示文档.assets/19.png)

![20](./imgs/ATM_Shopping_2.0演示文档.assets/20.png)

### 【5】添加商品（前提是列表已经被初始化至少一次）

![21](./imgs/ATM_Shopping_2.0演示文档.assets/21.png)

![22](./imgs/ATM_Shopping_2.0演示文档.assets/22.png)

![23](./imgs/ATM_Shopping_2.0演示文档.assets/23.png)

### 【6】删除商品

![24](./imgs/ATM_Shopping_2.0演示文档.assets/24.png)

### 【7】修改商品

![25](./imgs/ATM_Shopping_2.0演示文档.assets/25.png)

![26](./imgs/ATM_Shopping_2.0演示文档.assets/26.png)

### 【8】初始化商品列表

![21](./imgs/ATM_Shopping_2.0演示文档.assets/21.png)

### 【9】查看全部商品列表

![27](./imgs/ATM_Shopping_2.0演示文档.assets/27.png)

## 【三】购物车功能展示

```【】
===================购物车功能菜单=====================
                  1.添加商品
                  2.删除商品
                  3.结算购物车商品
                  4.查看当前购物车商品
======================欢迎使用=======================
```

### 【1】添加商品

![28](./imgs/ATM_Shopping_2.0演示文档.assets/28.png)

![29](./imgs/ATM_Shopping_2.0演示文档.assets/29.png)

![30](./imgs/ATM_Shopping_2.0演示文档.assets/30.png)

![31](./imgs/ATM_Shopping_2.0演示文档.assets/31.png)

### 【2】删除商品

![32](./imgs/ATM_Shopping_2.0演示文档.assets/32.png)

![33](./imgs/ATM_Shopping_2.0演示文档.assets/33.png)

### 【3】结算购物车商品

![34](./imgs/ATM_Shopping_2.0演示文档.assets/34.png)

![35](./imgs/ATM_Shopping_2.0演示文档.assets/35.png)

### 【4】查看当前购物车商品

![31](./imgs/ATM_Shopping_2.0演示文档.assets/31.png)