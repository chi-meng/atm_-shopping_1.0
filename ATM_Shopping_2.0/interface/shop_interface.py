# -*-coding: Utf-8 -*-
# @File : shop_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19


from db import db_hander
from interface import bank_interface


def get_goods_list():
    goods_data_dict = db_hander.select(username='goods', roles='shop')
    if not goods_data_dict:
        flow = f'当前没有初始化商品,请联系管理员初始化商品列表!!'
        return False, flow

    return True, goods_data_dict


def add_goods_interface(shop_car_now, goods_name, goods_price, ):
    # 购物车不存在该商品
    if goods_name not in shop_car_now:
        goods_number = 1
        shop_car_now[goods_name] = [goods_number, goods_price]

    # 存在商品 + 1
    goods_number = shop_car_now[goods_name][0] + 1
    shop_car_now[goods_name] = [goods_number, goods_price]
    return shop_car_now


def save_shop_car_interface(login_username, shop_car_after):
    user_data_dict = db_hander.select(login_username, 'user')
    user_data_dict['shopping_car'] = shop_car_after

    db_hander.save(login_username, user_data_dict, 'user')

    flow = f'商品添加购物车成功!!'

    return flow


def del_goods_interface(shop_car_now, del_goods_name, goods_price, del_goods_num):
    # 存在商品 + 1
    # # {"梦梦抱枕": [3, "180$"],}
    goods_number = int(shop_car_now[del_goods_name][0]) - del_goods_num
    shop_car_now[del_goods_name] = [goods_number, goods_price]
    if goods_number == 0:
        shop_car_now.pop(del_goods_name)
    return shop_car_now


def pay_goods_interface(login_username, shop_car_now, pay_goods_name, goods_price, pay_goods_num):
    # 数量 * 单价
    money_pay = float(float(goods_price) * float(pay_goods_num))
    # 银行付费
    flag, msg = bank_interface.shop_pay_for_goods_interface(login_username, money_pay)
    if flag:
        print(msg)
        goods_number = int(shop_car_now[pay_goods_name][0]) - pay_goods_num
        shop_car_now[pay_goods_name] = [goods_number, goods_price]
        if goods_number == 0:
            shop_car_now.pop(pay_goods_name)
            save_shop_car_interface(login_username, shop_car_now)
        return shop_car_now
    else:
        print(msg)
        return shop_car_now
