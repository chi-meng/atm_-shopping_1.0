# -*-coding: Utf-8 -*-
# @File : admin_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19

from lib import common
from db import db_hander


def register_interface(username, encrypt_password, salt):
    # 提供一个唯一ID卡号
    bank_code = common.get_bank_code()
    user_data_dict = {
        'username': username,
        'password': encrypt_password,
        'salt': salt,
        'bank_code': bank_code,
        'log_info': [],
        'log_goods_info': [],
    }

    flag, msg = db_hander.save(username=username, data_dict=user_data_dict, roles='admin')

    return flag, msg


def init_goods_list_interface(login_username, goods_dict):
    db_hander.save(username='goods', data_dict=goods_dict, roles='shop')

    flow = f'商品列表初始化成功!'

    return flow


def change_balance_interface(login_username, to_username, to_bank_id, to_balance):
    # 目标用户对象
    user_data_dict_to = db_hander.select(to_username, 'user')
    # 管理员对象
    user_data_dict_me = db_hander.select(login_username, 'admin')

    if not user_data_dict_to:
        flow = f'当前用户:>>>{to_username} 不存在'
        return False, flow

    if int(to_bank_id) != int(user_data_dict_to['bank_code']):
        flow = f'当前输入银行卡号与目标银行卡号不符!!!请确认信息!!'
        user_data_dict_me['log_info'].append(flow)
        return False, flow

    user_data_dict_to['balance'] = to_balance
    flow = f'当前管理员:>>>{login_username} 修改目标用户:>>>{to_username} 修改金额:>>>{to_balance} 成功!!'
    user_data_dict_me['log_info'].append(flow)

    db_hander.save(to_username, user_data_dict_to, 'user')
    db_hander.save(login_username, user_data_dict_me, 'admin')

    return True, flow


def change_user_locked_interface(login_username, to_username):
    # 目标用户对象
    user_data_dict_to = db_hander.select(to_username, 'user')
    # 管理员对象
    user_data_dict_me = db_hander.select(login_username, 'admin')

    if not user_data_dict_to:
        flow = f'当前用户:>>>{to_username} 不存在'
        return False, flow

    user_data_dict_to['locked'] = True
    flow = f'当前管理员:>>>{login_username} 修改目标用户:>>>{to_username} 冻结状态修改:>>>{user_data_dict_to["locked"]} 成功!!'
    user_data_dict_me['log_info'].append(flow)

    db_hander.save(to_username, user_data_dict_to, 'user')
    db_hander.save(login_username, user_data_dict_me, 'admin')

    return True, flow


def add_goods_interface(login_username, goods_num, goods_name, goods_price):
    goods_data_dict = db_hander.select('goods', 'shop')
    # 管理员对象
    user_data_dict_me = db_hander.select(login_username, 'admin')

    if goods_num in goods_data_dict.keys():
        flow = f'当前商品编号已存在,请重新输入!!'
        user_data_dict_me['log_goods_info'].append(flow)
        return False, flow

    goods_data_dict[goods_num] = [goods_name, goods_price]
    db_hander.save('goods', goods_data_dict, 'shop')

    flow = f'当前商品编号:>>>{goods_num} 商品名称:>>>{goods_name} 商品价格:>>>{goods_price} 添加成功!!'
    user_data_dict_me['log_goods_info'].append(flow)

    db_hander.save(login_username, user_data_dict_me, 'admin')
    return True, flow


def check_goods_list_interface():
    goods_data_dict = db_hander.select('goods', 'shop')
    # 管理员对象
    print('============================ 所有商品列表 ==============================')
    for index, goods in goods_data_dict.items():
        print(f'''当前商品编号:>>>{index}  商品名称:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
    print('============================ 感谢使用 =================================')


def del_goods_interface(login_user_name, goods_num):
    goods_data_dict = db_hander.select('goods', 'shop')
    # 管理员对象
    user_data_dict_me = db_hander.select(login_user_name, 'admin')

    goods_name = goods_data_dict[f'{goods_num}'][0]
    goods_price = goods_data_dict[f'{goods_num}'][1]
    # goods_dict = {'1': ['梦梦抱枕', '180'],}
    if goods_num not in goods_data_dict.keys():
        flow = f'当前商品编号不存在,请重新输入!!'
        user_data_dict_me['log_goods_info'].append(flow)
        return False, flow

    del goods_data_dict[f'{goods_num}']
    db_hander.save('goods', goods_data_dict, 'shop')

    flow = f'当前商品编号:>>>{goods_num} 商品名称:>>>{goods_name} 商品价格:>>>{goods_price} 删除成功!!'
    user_data_dict_me['log_goods_info'].append(flow)

    db_hander.save(login_user_name, user_data_dict_me, 'admin')
    return True, flow


def change_goods_interface(login_username, goods_num, goods_name, goods_price):
    goods_data_dict = db_hander.select('goods', 'shop')
    # 管理员对象
    user_data_dict_me = db_hander.select(login_username, 'admin')

    if goods_num not in goods_data_dict.keys():
        flow = f'当前商品编号不存在,请重新输入!!'
        user_data_dict_me['log_goods_info'].append(flow)
        return False, flow

    if goods_name == None:
        goods_name = goods_data_dict[f'{goods_num}'][0]

    if goods_price == None:
        goods_price = goods_data_dict[f'{goods_num}'][1]

    goods_data_dict[f'{goods_num}'] = [goods_name, goods_price]
    db_hander.save('goods', goods_data_dict, 'shop')

    flow = f'当前商品编号:>>>{goods_num} 商品名称:>>>{goods_name} 商品价格:>>>{goods_price} 修改成功!!'
    user_data_dict_me['log_goods_info'].append(flow)

    db_hander.save(login_username, user_data_dict_me, 'admin')
    return True, flow
