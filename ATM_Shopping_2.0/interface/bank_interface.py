# -*-coding: Utf-8 -*-
# @File : bank_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19

'''
银行接口
'''
from db import db_hander
from conf import settings


def user_insert_money_interface(login_username, money_input):
    user_data_dict = db_hander.select(login_username, 'user')

    balance_before = user_data_dict['balance']

    balance_after = balance_before + money_input

    user_data_dict['balance'] = balance_after

    flow = f'当前用户:>>>{login_username} 充值前余额:>>>{balance_before} 充值后余额:>>>{balance_after}'
    user_data_dict['record'].append(flow)
    db_hander.save(login_username, user_data_dict, roles='user')
    return True, flow


def user_get_money_interface(login_username, money_get):
    user_data_dict = db_hander.select(login_username, 'user')

    balance_before = user_data_dict['balance']

    if balance_before < money_get:
        flow = f'当前用户:>>>{login_username} 余额为:>>>{balance_before} 提现金额为:>>>{money_get} 余额不足!无法提现!'
        return False, flow

    balance_after = balance_before - money_get

    user_data_dict['balance'] = balance_after

    flow = f'当前用户:>>>{login_username} 提现前余额:>>>{balance_before} 提现后余额:>>>{balance_after}'
    user_data_dict['record'].append(flow)

    db_hander.save(login_username, user_data_dict, roles='user')
    return True, flow


def user_transfer_money_interface(login_username, money_to_user, money_to_bank_code, money_to_):
    # 手续费率 0.02
    poundage = settings.get_poundage(money_to_)

    # 用户数据
    user_data_dict_from = db_hander.select(login_username, 'user')
    user_data_dict_to = db_hander.select(money_to_user, 'user')

    if not user_data_dict_from or not user_data_dict_to:
        flow = f'账户不符,请再次确认!!'
        return False,flow

    # 登录用户余额
    balance_before_from = user_data_dict_from['balance']
    # 目标用户余额
    balance_before_to = user_data_dict_to['balance']
    # 目标用户银行卡号
    bank_code_to = user_data_dict_to['bank_code']

    # 校对银行卡号
    if bank_code_to != money_to_bank_code:
        flow = f'当前银行账号与目标账户账号不符!!请再次确认!'
        return False, flow



    # 金额大于余额
    if balance_before_from < (money_to_ * (1 + poundage)):
        flow = f'当前用户:>>>{login_username} 余额为:>>>{balance_before_from} 转账金额为:>>>{money_to_} 余额不足!无法提现!'
        return False, flow

    # 转账自身余额
    money_poundage = money_to_ * poundage
    balance_after_from = balance_before_from - money_to_ - money_poundage
    balance_after_to = balance_before_to + money_to_

    user_data_dict_from['balance'] = balance_after_from
    user_data_dict_to['balance'] = balance_after_to

    flow_from = f'当前用户:>>>{login_username} 转向目标用户:>>>{money_to_user} 转账前余额:>>>{balance_before_from} 转账后余额:>>>{balance_after_from} 手续费为:>>>{money_poundage}'
    user_data_dict_from['record'].append(flow_from)

    flow_to = f'当前用户:>>>{money_to_user} 收到目标用户:>>>{login_username} 转账前余额:>>>{balance_before_from} 转账后余额:>>>{balance_after_from} 手续费为:>>>{money_poundage}'
    user_data_dict_to['record'].append(flow_to)

    db_hander.save(login_username, user_data_dict_from, roles='user')
    db_hander.save(money_to_user, user_data_dict_to, roles='user')

    return True, flow_from


def shop_pay_for_goods_interface(login_username, money_pay):
    # 用户数据
    user_data_dict_from = db_hander.select(login_username, 'user')

    # 登录用户余额
    balance_before_from = float(user_data_dict_from['balance'])

    # 减去消费余额
    if money_pay > balance_before_from:
        flow = f'当前余额为:>>>{balance_before_from} 需付费:>>>{money_pay} 余额不足,请先充值!!'
        return False, flow

    user_data_dict_from['balance'] -= money_pay

    flow_to = f'当前用户:>>>{login_username} 消费:>>>{money_pay} 消费前余额:>>>{balance_before_from} 转账后余额:>>>{user_data_dict_from["balance"]}'
    user_data_dict_from['record'].append(flow_to)

    db_hander.save(login_username, user_data_dict_from, roles='user')

    return True, flow_to
