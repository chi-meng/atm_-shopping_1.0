# -*-coding: Utf-8 -*-
# @File : user_interface .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19


'''
用户面板层
'''
from db import db_hander
from interface import bank_interface
from lib import common
from conf import settings

logger = settings.get_logger('user')


def register_interface(username, encrypt_password, salt):
    # 提供一个随机银行卡号
    bank_code = common.get_bank_code()
    user_data_dict = {
        'username': username,
        'password': encrypt_password,
        'salt': salt,
        'bank_code': bank_code,
        'balance': 10000,
        'locked': False,
        'record': [],
        'shopping_car': {},
    }

    flag, msg = db_hander.save(username=username, data_dict=user_data_dict, roles='user')

    return flag, msg


def check_locked_interface(username):
    user_data_dict = db_hander.select(username, 'user')
    user_locked = user_data_dict['locked']
    if user_locked:
        flow = f'当前用户已被锁定!请联系管理员进行解除!'
        logger.info(flow)
        return False, flow

    return True, None


def get_shopping_car(login_username):
    user_data_dict = db_hander.select(login_username, 'user')
    return user_data_dict['shopping_car']


def insert_money_interface(login_username, money_input):
    flag, msg = bank_interface.user_insert_money_interface(login_username, money_input)

    return flag, msg


def check_balance_interface(login_username):
    user_data_dict = db_hander.select(login_username)

    balance = user_data_dict['balance']

    return balance


def get_balance_interface(login_username, money_get):
    flag, msg = bank_interface.user_get_money_interface(login_username, money_get)

    return flag, msg


def transfer_money_interface(login_username, money_to_user, money_to_bank_code, money_to_):
    flag, msg = bank_interface.user_transfer_money_interface(login_username, money_to_user, money_to_bank_code,
                                                             money_to_)

    return flag, msg


def check_info_details_interface(login_username):
    user_data_dict = db_hander.select(login_username, 'user')

    return user_data_dict
