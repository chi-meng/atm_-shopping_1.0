# -*-coding: Utf-8 -*-
# @File : shopping_car .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19

from interface import shop_interface
from lib import common
from core import src
from conf import settings

logger = settings.get_logger('shop')


@common.login_auth('user')
def add_goods_to_car():
    '''
    {"0": ["商品名称", "商品价格"], "1": ["梦梦抱枕", "180$"],}
    :return:
    '''
    shop_car_now = src.user.login_dict['shopping_car']
    shop_car_after = {}
    while True:
        flag, goods_data_dict = shop_interface.get_goods_list()

        if not flag:
            print(goods_data_dict)

        print('============================ 选购商品列表 ==============================')
        for index, goods in goods_data_dict.items():
            print(f'''当前商品编号:>>>{index}  商品名称:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
        print('============================ 感谢使用 ==============================')
        goods_chose = input(f'请输入商品编号:>>>>').strip()

        if goods_chose == 'q':
            flow = shop_interface.save_shop_car_interface(src.user.login_dict['username'], shop_car_after)
            print(flow)
            logger.info(flow)
            break

        if not goods_chose.isdigit():
            print(f'请输入正确的编号!')
            continue

        goods_chose = int(goods_chose)

        if goods_chose not in range(len(goods_data_dict)):
            print(f'此编号不在范围内,请重新输入!!')
            continue

        goods_data = goods_data_dict[f'{goods_chose}']
        goods_name = goods_data[0]
        goods_price = goods_data[1]

        shop_car_after = shop_interface.add_goods_interface(shop_car_now, goods_name, goods_price, )

        print('============================ 已购商品列表 ==============================')
        for index, goods in shop_car_after.items():
            print(f'''当前已购商品名称:>>>{index}  已购商品数量:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
        print('============================ 感谢消费 ==============================')


@common.login_auth('user')
def del_goods_to_car():
    # 初始化购物车列表
    shop_car_now = src.user.login_dict['shopping_car']
    shop_car_after = {}
    while True:

        if not shop_car_now:
            print(f'当前购物车为空,请先添加商品!!谢谢!!!')

        # {"梦梦抱枕": [3, "180$"],}
        print('============================ 已购商品列表 ==============================')
        for index, goods in shop_car_now.items():
            print(f'''当前商品名称:>>>{index}  商品数量:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
        print('============================ 感谢使用 ==============================')

        del_goods_name = input(f'请输入想要删除的商品名称:>>>>').strip()

        if del_goods_name == 'q':
            flow = shop_interface.save_shop_car_interface(src.user.login_dict['username'], shop_car_after)
            print(flow)
            logger.info(flow)
            break

        del_goods_num = input(f'请输入想要删除的商品数量:>>>>').strip()

        if not del_goods_num.isdigit():
            print(f'请输入正确的编号!')
            continue

        del_goods_num = int(del_goods_num)

        goods_data = shop_car_now[f'{del_goods_name}']
        goods_name = goods_data[0]
        goods_price = goods_data[1]

        if del_goods_num > goods_name:
            print(f'超出删除范围,不允许删除')
            continue

        shop_car_after = shop_interface.del_goods_interface(shop_car_now, del_goods_name, goods_price, del_goods_num)


@common.login_auth('user')
def pay_goods_to_car():
    # 初始化购物车列表
    shop_car_now = src.user.login_dict['shopping_car']
    shop_car_after = {}
    while True:

        if not shop_car_now:
            print(f'当前购物车为空,请先添加商品!!谢谢!!!')

        # {"梦梦抱枕": [3, "180$"],}
        print('============================ 已购商品列表 ==============================')
        for index, goods in shop_car_now.items():
            print(f'''当前商品名称:>>>{index}  商品数量:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
        print('============================ 感谢使用 ==============================')

        pay_goods_name = input(f'请输入想要结算的商品名称:>>>>').strip()

        if pay_goods_name == 'q':
            flow = shop_interface.save_shop_car_interface(src.user.login_dict['username'], shop_car_after)
            print(flow)
            logger.info(flow)
            break

        pay_goods_num = input(f'请输入想要结算的商品数量:>>>>').strip()

        if not pay_goods_num.isdigit():
            print(f'请输入正确的编号!')
            continue

        pay_goods_num = int(pay_goods_num)

        # # {"梦梦抱枕": [3, "180$"],}
        goods_data = shop_car_now[f'{pay_goods_name}']
        goods_name = goods_data[0]
        goods_price = goods_data[1]

        if pay_goods_num > goods_name:
            print(f'超出删除范围,不允许结算')
            continue

        shop_car_after = shop_interface.pay_goods_interface(src.user.login_dict['username'], shop_car_now,
                                                            pay_goods_name, goods_price, pay_goods_num)


@common.login_auth('user')
def check_goods_to_car():
    # 初始化购物车列表
    shop_car_now = src.user.login_dict['shopping_car']
    shop_car_after = {}
    if not shop_car_now:
        print(f'当前购物车为空,请先添加商品!!谢谢!!!')
    # {"梦梦抱枕": [3, "180$"],}
    print('============================ 已购商品列表 ==============================')
    for index, goods in shop_car_now.items():
        print(f'''当前商品名称:>>>{index}  商品数量:>>>{goods[0]}  商品价格:>>>{goods[1]}$''')
    print('============================ 感谢使用 ==============================')


func_menus = '''
===================购物车功能菜单=====================
                  1.添加商品
                  2.删除商品
                  3.结算购物车商品
                  4.查看当前购物车商品
======================欢迎使用=======================
'''

func_dict = {
    '1': add_goods_to_car,
    '2': del_goods_to_car,
    '3': pay_goods_to_car,
    '4': check_goods_to_car,
}


def main():
    while True:
        print(func_menus)

        choice_input = input('请输入功能ID:>>>>').strip()

        if choice_input == 'q':
            break

        if not choice_input.isdigit():
            print(f'请重新确认信息!')
            continue

        if choice_input not in func_dict.keys():
            print(f'请输入正确的功能ID!!')
            continue

        func_dict[choice_input]()
