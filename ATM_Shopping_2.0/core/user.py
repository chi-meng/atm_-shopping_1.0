# -*-coding: Utf-8 -*-
# @File : user .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19

'''
普通用户视图
'''
from lib import common
from interface import user_interface, bank_interface
from conf import settings

logger = settings.get_logger('user')

login_dict = {
    'username': None,
    'locked': False,
    'shopping_car': None
}


def register():
    while True:
        username = input('请输入用户名:>>>>').strip()

        if username == 'q':
            break

        password = input('请输入密码:>>>>').strip()
        re_password = input('请再次确认密码:>>>>').strip()

        if password != re_password:
            print(f'请再次确认你的密码是否一致!!')
            continue

        salt = common.get_verify_code(6)
        encrypt_password = common.encrypt_data(password, salt)

        flag, msg = user_interface.register_interface(username, encrypt_password, salt)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)


def login():
    while True:
        username = input('请输入用户名:>>>>').strip()

        if username == 'q':
            break

        password = input('请输入密码:>>>>').strip()
        login_code = common.get_verify_code(4)
        login_code_input = input(f'请输入验证码进行登陆{login_code}:>>>>').strip()

        flag, msg = user_interface.check_locked_interface(username)

        if not flag:
            print(msg)
            logger.info(msg)
            break

        flag, msg = common.login_common(
            username=username, password=password, login_code=login_code, login_code_input=login_code_input,
            roles='user')

        if flag:
            print(msg)
            login_dict['username'] = username
            login_dict['shopping_car'] = user_interface.get_shopping_car(username)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)
            continue


@common.login_auth('user')
def insert_balance():
    while True:
        money_input = input('请输入需要充值的金额').strip()

        if money_input == 'q':
            break

        if not money_input.isdigit():
            print(f'请输入正确的金额!!')

        money_input = int(money_input)

        if money_input < 0:
            print(f'请确认当前金额,必须大于0 !!')
            continue

        flag, msg = user_interface.insert_money_interface(login_dict['username'], money_input)

        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)


@common.login_auth('user')
def get_balance():
    while True:
        money_get = input('请输入需要提取的金额:>>>>').strip()
        if not money_get.isdigit():
            print(f'请输入正确的金额!!')

        money_get = int(money_get)

        if money_get < 0:
            print(f'请确认当前金额,必须大于0 !!')
            continue

        flag, msg = user_interface.get_balance_interface(login_dict['username'], money_get)

        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)


@common.login_auth('user')
def check_balance():
    balance = user_interface.check_balance_interface(login_dict['username'])
    print(f'当前用户:>>>{login_dict["username"]}  余额为:>>>{balance}')


@common.login_auth('user')
def transfer_money():
    while True:
        money_to_user = input('请输入对方的用户名:>>>>').strip()

        if money_to_user == 'q':
            break

        money_to_bank_code = input('请输入对方的银行卡号:>>>>').strip()
        money_to_ = input('请输入转账的金额:>>>>').strip()

        if not money_to_bank_code.isdigit():
            print(f'输入的金额不合法! 请重新确认!')
            continue

        money_to_ = int(money_to_)

        if money_to_ < 0:
            print(f'金额必须大于0!请重新确认!')
            continue

        flag, msg = user_interface.transfer_money_interface(login_dict['username'], money_to_user, money_to_bank_code,
                                                            money_to_)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)


@common.login_auth('user')
def check_info_details():
    user_data_dict = user_interface.check_info_details_interface(login_dict['username'])
    username = login_dict['username']
    balance = user_data_dict['balance']
    bank_code = user_data_dict['bank_code']
    print(f'''
    ======================当前用户详细信息如下=========================
                        当前用户名:>>>{username}
                        当前银行余额:>>>{balance}
                        当前银行卡号:>>>{bank_code}
    =====================欢迎使用该系统,万分感谢=======================
    ''')


@common.login_auth('user')
def check_records():
    user_data_dict = user_interface.check_info_details_interface(login_dict['username'])
    records = user_data_dict['record']

    for index, record in enumerate(records):
        print(f'当前编号:>>>{index}  流水内容为:>>>{record}')


func_menus = '''
===================用户功能菜单=====================
                  1.注册
                  2.登陆
                  3.取款
                  4.转账
                  5.充值余额
                  6.查看流水
                  7.查看银行信息
======================欢迎使用=======================
'''

func_dict = {
    '1': register,
    '2': login,
    '3': get_balance,
    '4': transfer_money,
    '5': insert_balance,
    '6': check_records,
    '7': check_info_details,

}


def main():
    while True:
        print(func_menus)

        choice_input = input('请输入功能ID:>>>>').strip()

        if choice_input == 'q':
            break

        if not choice_input.isdigit():
            print(f'请重新确认信息!')
            continue

        if choice_input not in func_dict.keys():
            print(f'请输入正确的功能ID!!')
            continue

        func_dict[choice_input]()
