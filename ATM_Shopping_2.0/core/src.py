# -*-coding: Utf-8 -*-
# @File : src .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19

from core import admin, user, shopping_car

func_menus = '''
===================ATM + 购物车=====================
                    1.管理员功能
                    2.用户功能
                    3.购物功能
======================欢迎使用=======================
'''

func_dict = {
    '1': admin.main,
    '2': user.main,
    '3': shopping_car.main,

}


def main():
    while True:
        print(func_menus)

        choice_input = input('请输入功能ID:>>>>').strip()

        if choice_input == 'q':
            break

        if not choice_input.isdigit():
            print(f'请重新确认信息!')
            continue

        if choice_input not in func_dict.keys():
            print(f'请输入正确的功能ID!!')
            continue

        func_dict[choice_input]()
