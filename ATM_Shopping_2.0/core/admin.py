# -*-coding: Utf-8 -*-
# @File : admin .py
# author: Chimengmeng
# blog_url : https://www.cnblogs.com/dream-ze/
# Time：2023/6/19


'''
管理员功能
'''
from interface import admin_interface
from lib import common
from conf import settings

logger = settings.get_logger('admin')

login_dict = {
    'username': None,
    'locked': False,
}


def register():
    while True:
        username = input('请输入用户名:>>>>').strip()

        if username == 'q':
            break

        password = input('请输入密码:>>>>').strip()
        re_password = input('请再次确认密码:>>>>').strip()

        if password != re_password:
            print(f'请再次确认你的密码是否一致!!')
            continue

        salt = common.get_verify_code(6)
        encrypt_password = common.encrypt_data(password, salt)

        flag, msg = admin_interface.register_interface(username, encrypt_password, salt)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)


def login():
    while True:
        username = input('请输入用户名:>>>>').strip()

        if username == 'q':
            break

        password = input('请输入密码:>>>>').strip()
        login_code = common.get_verify_code(4)
        login_code_input = input(f'请输入验证码进行登陆{login_code}:>>>>').strip()
        flag, msg = common.login_common(
            username=username, password=password, login_code=login_code, login_code_input=login_code_input,
            roles='admin')

        if flag:
            print(msg)
            login_dict['username'] = username
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)
            continue


@common.login_auth('admin')
def change_balance():
    while True:
        to_username = input('请输入目标用户的用户名:>>>>').strip()
        to_bank_id = input('请输入目标用户的银行卡号:>>>>').strip()
        to_balance = input('请输入需要修改的金额:>>>>').strip()

        if not to_bank_id.isdigit() or not to_balance.isdigit():
            print('请输入合法的银行卡号或目标金额!!!')

        to_bank_id = int(to_bank_id)
        to_balance = int(to_balance)

        flag, msg = admin_interface.change_balance_interface(login_dict['username'], to_username, to_bank_id,
                                                             to_balance)
        if not flag:
            print(msg)
            logger.warning(msg)
            continue
        else:
            print(msg)
            logger.info(msg)
            break


@common.login_auth('admin')
def change_user_locked():
    while True:
        to_username = input('请输入目标用户的用户名:>>>>').strip()

        flag, msg = admin_interface.change_user_locked_interface(login_dict['username'], to_username)
        if not flag:
            print(msg)
            logger.warning(msg)
            continue
        else:
            print(msg)
            logger.info(msg)
            break


@common.login_auth('admin')
def init_goods_list():
    while True:
        goods_dict = {
            '1': ['梦梦抱枕', '180'],
            '2': ['梦梦睡衣', '120'],
            '3': ['梦梦笔记本', '40'],
            '4': ['梦梦水杯', '20'],
            '5': ['梦梦毛绒玩具', '60'],
            '6': ['梦梦围巾', '30'],
            '7': ['梦梦手链', '25'],
            '8': ['梦梦钥匙扣', '15'],
            '9': ['梦梦手机壳', '35'],
            '10': ['梦梦抱枕套装', '200']
        }

        admin_id = input('请输入超级密码进行重置:>>>>').strip()

        if admin_id == 'q':
            break

        if not admin_id.isdigit():
            print(f'请输入正确数字代码!!!')

        admin_id = int(admin_id)

        if admin_id != 1314521:
            flow = f'超级密码错误!无重置权限!'
            logger.warning(flow)
            continue

        flow = admin_interface.init_goods_list_interface(login_dict['username'], goods_dict)

        print(flow)
        logger.info(flow)
        break


@common.login_auth('admin')
def add_goods():
    while True:
        admin_interface.check_goods_list_interface()
        goods_num = input('请输入商品编号:>>>>').strip()
        goods_name = input('请输入商品名称:>>>>').strip()
        goods_price = input('请输入商品价格:>>>>').strip()

        if not goods_num.isdigit() or not goods_price.isdigit():
            print('请输入合法的商品编号或商品价格!!!')

        flag, msg = admin_interface.add_goods_interface(login_dict['username'], goods_num, goods_name, goods_price)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)
            continue


@common.login_auth('admin')
def del_goods():
    while True:
        admin_interface.check_goods_list_interface()
        goods_num = input('请输入删除目标商品编号:>>>>').strip()

        if not goods_num.isdigit():
            print('请输入合法的商品编号!!!')

        flag, msg = admin_interface.del_goods_interface(login_dict['username'], goods_num)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)
            continue


@common.login_auth('admin')
def change_goods():
    while True:
        goods_name = None
        goods_price = None
        admin_interface.check_goods_list_interface()
        goods_num = input('请输入修改的商品编号:>>>>').strip()
        func_menus = '''
        ----------------参数参考------------------
                    1.修改商品名称
                    2.修改商品价格
                    3.修改商品名称 + 商品价格
                    4.使用默认名称和价格  
        ----------------------------------
        '''
        choice_name_price = input(f'请选择修改的参数{func_menus}:>>>>')

        if choice_name_price == '1':
            goods_name = input('请输入修改的商品名称:>>>>').strip()
            if not goods_num.isdigit():
                print('请输入合法的商品编号或商品价格!!!')
                continue
        elif choice_name_price == '2':
            goods_price = input('请输入修改的商品价格:>>>>').strip()
            if not goods_price.isdigit():
                print('请输入合法的商品编号或商品价格!!!')
                continue
        elif choice_name_price == '3':
            goods_name = input('请输入修改的商品名称:>>>>').strip()
            goods_price = input('请输入修改的商品价格:>>>>').strip()
            if not goods_num.isdigit() or not goods_price.isdigit():
                print('请输入合法的商品编号或商品价格!!!')
                continue

        flag, msg = admin_interface.change_goods_interface(login_dict['username'], goods_num, goods_name, goods_price)
        if flag:
            print(msg)
            logger.info(msg)
            break
        else:
            print(msg)
            logger.warning(msg)
            continue


@common.login_auth('admin')
def check_goods_list():
    admin_interface.check_goods_list_interface()


func_menus = '''
===================管理员功能菜单=====================
                  1.注册
                  2.登陆
                  3.冻结用户
                  4.修改用户余额
                  5.添加商品
                  6.删除商品
                  7.修改商品
                  8.初始化商品列表
                  9.查看全部商品列表
======================欢迎使用=======================
'''

func_dict = {
    '1': register,
    '2': login,
    '3': change_user_locked,
    '4': change_balance,
    '5': add_goods,
    '6': del_goods,
    '7': change_goods,
    '8': init_goods_list,
    '9': check_goods_list,

}


def main():
    while True:
        print(func_menus)

        choice_input = input('请输入功能ID:>>>>').strip()

        if choice_input == 'q':
            break

        if not choice_input.isdigit():
            print(f'请重新确认信息!')
            continue

        if choice_input not in func_dict.keys():
            print(f'请输入正确的功能ID!!')
            continue

        func_dict[choice_input]()
